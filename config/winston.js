var winston = require('winston');
var moment = require('moment');

var options = {
  mallOrder:{
      level: 'debug',
      filename: `./logs/mallorder/${moment().format('Y-MM-DD')}.log`,
      handleExceptions: true,
      json: true,
      maxsize: 5242880, // 5MB
      maxFiles: 5,
      colorize: false,
      timestamp:false
    },
    wsReceive:{
      level: 'debug',
      filename: `./logs/wsReceive/${moment().format('Y-MM-DD')}.log`,
      handleExceptions: true,
      json: true,
      maxsize: 5242880, // 5MB
      maxFiles: 5,
      colorize: false,
      timestamp:false
    }
  };




  

//   var logger = new winston.Logger({
//     transports: [
//       new winston.transports.File(options.file),
//       new winston.transports.Console(options.console)
//     ],
//     exitOnError: false, // do not exit on handled exceptions
//   });


//   logger.stream = {
//     write: function(message, encoding) {
//       // use the 'info' log level so the output will be picked up by both transports (file and console)
//       logger.info(message);
//     },
//   };
  
// module.exports.logger = logger;

  var mallPushLogger = new winston.createLogger({
    transports: [
      new winston.transports.File(options.mallOrder),
    ],
    exitOnError: false, // do not exit on handled exceptions
  });

module.exports.mallPushLogger = mallPushLogger;


var wsMallPushLogger = new winston.createLogger({
  transports: [
    new winston.transports.File(options.wsReceive),
  ],
  exitOnError: false, // do not exit on handled exceptions
});

module.exports.wsMallPushLogger = wsMallPushLogger;
  