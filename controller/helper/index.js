
 const MarketOrder = require('../../models').ConsumerMarketOrder;


 
const ConsumerVoucher = require('../../models').ConsumerVoucher;
const SpecialPackage   = require('../../models').SpecialPackage;



 const moment = require('moment');
 
 const  queue = require('kue').createQueue();
 
 
 module.exports.rebateTransaction = async function (order_id) {
 
 
               
     var where = {order_id: order_id}
 
     var Market_Order
     [err, Market_Order] = await to(MarketOrder.findOne({
         where: where
     }));
 
 
             
     var total_rebate = parseFloat(Market_Order.total_payment)-parseFloat(Market_Order.total_shipping_fee)
     console.log("total rebate ",total_rebate)
 
 
     var data=[]
     data.push({
         trans_type: "market", 
         order_id:order_id, 
         amount:total_rebate,
     })
 
 
     
 
     if(Market_Order.voucher_list)
     {
        
         var voucher_list = JSON.parse(Market_Order.voucher_list)
         console.log("rebate voucher ",voucher_list)
 
         var voucher_id_list =[]
         Object.entries(voucher_list).forEach(([key, value])=>{
             voucher_id_list.push(key)
         })
 
         ConsumerVoucher.findAll({
             where: {voucher_id:voucher_id_list},
             include:[{model:SpecialPackage,attributes:['package_title','package_price','item_price']}],
             attributes:['voucher_id']
         }).then((voucher_detail) => {
 
             for(var i =0;i<voucher_detail.length;i++)
             {
                  
 
                 data.push({
                     trans_type: "voucher",
                     voucher_name:voucher_detail[i].SpecialPackage.package_title, 
                     voucher_id:voucher_detail[i].voucher_id,
                     amount:voucher_detail[i].SpecialPackage.item_price,
                     item_price:voucher_detail[i].SpecialPackage.item_price,
                     package_price:voucher_detail[i].SpecialPackage.package_price,
                     lolol_voucher:true
                 })
 
                 
 
             }
 
                 var rebate = {
                     event:"payment",
                     consumer:Market_Order.consumer_id,
                     data:data
                 }
 
             queue.create('event_hook', rebate).removeOnComplete( true ).save()
 
 
         });
 
         
     }else {
 
     var rebate = {
             event:"payment",
             consumer:Market_Order.consumer_id,
             data:data
         }
 
      queue.create('event_hook', rebate).removeOnComplete( true ).save()
 
 
     }
 
 
 
 }
 
 