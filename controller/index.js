const moment = require('moment');
// const ConsumerOrder = require('../../models').ConsumerOrder;
// const restaurant = require('../../models').restaurant;
// const Merchant = require('../../models').merchant;
 const sequelize = require('../models').sequelize;

var pushNotification = require('./pushNotification');

 const MarketOrder = require('../models').ConsumerMarketOrder;

 const MarketOrderTime = require('../models').ConsumerMarketOrderTime;




 //const  queue = require('kue').createQueue();

 var helper = require("./helper");


var test = function () {

    console.log("test run")
}

exports.test = test;


var lolisticCompleteOrder = async function () {

    console.log("run lolisticCompleteOrder")
    
    var t = 'YYYY-MM-DD HH:mm:ss';

    var s = moment().format(t);
    var e = moment().add(59, 's').format(t);

    var now = moment().format(t)
    var review_expired = moment().add(14, 'days').format(t)
    //  s = '2019-04-23 13:08:00'
    //  e = '2019-04-23 13:09:00'

    sequelize.query("select * from (SELECT  `ConsumerMarketOrder`.`order_id`, `ConsumerMarketOrder`.`consumer_id`, `ConsumerMarketOrder`.`status`,  `ConsumerMarketOrder`.`market_refund_id`, `ConsumerMarketOrder`.`order_date`,  `ConsumerMarketOrder`.`carrier_company`, `ConsumerMarketOrder`.`createdAt`, `ConsumerMarketOrder`.`updatedAt`, `MarketRefund`.`market_refund_id` AS `MarketRefund.market_refund_id`, `MarketRefund`.`status` AS `MarketRefund.status`, `ShippingService`.`shipping_company`, `ShippingService`.`shipping_key`, `ShippingService`.`mall_shipping_service` ,`ShippingService`.`auto_complete_day`, `ConsumerMarketOrderTime`.`time_id` AS `ConsumerMarketOrderTime.time_id`, `ConsumerMarketOrderTime`.`order_id` AS `ConsumerMarketOrderTime.order_id`, `ConsumerMarketOrderTime`.`place_order_time` AS `ConsumerMarketOrderTime.place_order_time`, `ConsumerMarketOrderTime`.`payment_time` AS `ConsumerMarketOrderTime.payment_time`, `ConsumerMarketOrderTime`.`ship_time` AS `ConsumerMarketOrderTime.ship_time`, `ConsumerMarketOrderTime`.`estimated_time` AS `ConsumerMarketOrderTime.estimated_time`, `ConsumerMarketOrderTime`.`awaiting_delivery_time` AS `ConsumerMarketOrderTime.awaiting_delivery_time`, `ConsumerMarketOrderTime`.`in_delivery_time` AS `ConsumerMarketOrderTime.in_delivery_time`, `ConsumerMarketOrderTime`.`order_receive_time` + interval `ShippingService`.`auto_complete_day` DAY as auto_complete_date,`MarketRefund`.`consumer_cancel_time`  + interval `ShippingService`.`auto_complete_day` DAY as con_cxl_delay_complete_date,`MarketRefund`.`admin_cancel_time`  + interval `ShippingService`.`auto_complete_day` DAY as admin_cxl_delay_complete_date,`MarketRefund`.`admin_complete_time`  + interval `ShippingService`.`auto_complete_day` DAY as admin_delay_complete_date, `ConsumerMarketOrderTime`.`order_receive_time` AS `ConsumerMarketOrderTime.order_receive_time`, `ConsumerMarketOrderTime`.`cancel_time` AS `ConsumerMarketOrderTime.cancel_time`, `ConsumerMarketOrderTime`.`refund_time` AS `ConsumerMarketOrderTime.refund_time`, `ConsumerMarketOrderTime`.`complete_time` AS `ConsumerMarketOrderTime.complete_time` FROM `market_order` AS `ConsumerMarketOrder` LEFT OUTER JOIN `market_refund` AS `MarketRefund` ON `ConsumerMarketOrder`.`market_refund_id` = `MarketRefund`.`market_refund_id` INNER JOIN `shipping_service` AS `ShippingService` ON `ConsumerMarketOrder`.`carrier_company` = `ShippingService`.`shipping_key` AND `ShippingService`.`mall_shipping_service` = 1 INNER JOIN `market_order_time` AS `ConsumerMarketOrderTime` ON `ConsumerMarketOrder`.`order_id` = `ConsumerMarketOrderTime`.`order_id` AND `ConsumerMarketOrderTime`.`order_receive_time` is not null  WHERE  `ConsumerMarketOrder`.`status` = 2 ) as innertable where (auto_complete_date BETWEEN '"+s+"' AND '"+e+"' AND market_refund_id is null ) or ( `MarketRefund.status` > 5 AND con_cxl_delay_complete_date BETWEEN '"+s+"' AND '"+e+"' or admin_cxl_delay_complete_date BETWEEN '"+s+"' AND '"+e+"' or admin_delay_complete_date BETWEEN '"+s+"' AND '"+e+"')", {
            type: sequelize.QueryTypes.SELECT
        }).then(orderList => {


            
            // for(var i =0;i<orderList.length;i++)
            // {  // if(orderList[i].MarketRefund.status)
                
            //     if(orderList[i]['MarketRefund.status']!=1&&orderList[i]['MarketRefund.status']!=2)
            //     {  
            //          console.log("asda sd a sd ",JSON.stringify(orderList[i]))
            //         console.log("refund status",orderList[i]['MarketRefund.status'])
            //     }
            //     else
            //     {
            //         console.log("refund processing or refund complete")
            //     }

            // }

            
        orderList.forEach(async function (orderDetail) {
            if(orderDetail['MarketRefund.status']>5||orderDetail['MarketRefund.status']==null)
            {  
                 console.log("asda sd a sd ",JSON.stringify(orderDetail))
                console.log("refund status",orderDetail['MarketRefund.status'])


                var where = {order_id: orderDetail.order_id}

                 var err, Update_Consumer_Order
                [err, Update_Consumer_Order] = await to( MarketOrder.update({
                        status: 3,
                        complete_time:now,
                        product_review_expire_time:review_expired,
                        completed_by_system:1
                    }, {
                        where: where
                    })
                );

                console.log("err",JSON.stringify(err))
                console.log("Update_Consumer_Order",JSON.stringify(Update_Consumer_Order))

               var Update_MarketOrder_Time
                [err, Update_MarketOrder_Time] = await to( MarketOrderTime.update({
                        complete_time:now
                    }, {
                        where:where
                    })
                );

                console.log("err",JSON.stringify(err))
                console.log("Update_MarketOrder_Time",JSON.stringify(Update_MarketOrder_Time))


                var rebate_transaction = await helper.rebateTransaction(orderDetail.order_id)
  


                let msg = `Your Order #${orderDetail.order_id} has been completed`
            
                var notiData = {}
                notiData.order_id = orderDetail.order_id
                notiData.status = 3

                pushNotification.handleNotification(true, orderDetail.consumer_id, orderDetail.order_id, "consumer", "mall_order", msg, null, "Complete Order", true, "order_completed", JSON.stringify(notiData))

            }
            else
            {
                console.log("refund processing or refund complete")
            }

    
       })



        }).catch(err => {
            console.log(err);
        })

    // sequelize.query("select * from (SELECT id,consumer_order.restaurant_id,order_type,order_date - interval food_preparation minute as deli_remind,merchant_id FROM consumer_order INNER JOIN merchant ON merchant.permission LIKE CONCAT('%\"', restaurant_id,'\":{%\"manage_kot\":true%}%') INNER JOIN restaurant_setting ON consumer_order.restaurant_id=restaurant_setting.restaurant_id  where status = 2  AND order_type IN ('delivery') order by order_date)as innertable where deli_remind BETWEEN '"+s+"' AND '"+e+"'", {
    //     type: sequelize.QueryTypes.SELECT
    // }).then(orderList => {


    //     console.log("asda sd a sd ",JSON.stringify(orderList))


    // }).catch(err => {
    //     console.log(err);
    // })

    // where = {consumer_id:4203}
    // var err, Consumer_Order
    // [err, Consumer_Order] = await to(MarketOrder.findAll({
    //     where: where,
    //     //,include: [{ model:MarketShop,where:{shop_id:req.params.shop_id,is_verify:1}}]               
    //     limit: 10,
    //     include: [ {
    //         model: MarketRefund,
    //         where:{status:2},
    //         attributes:['market_refund_id','status']
    //     }, {
    //         model: ShippingService,
    //         where:{mall_shipping_service:1},
    //         attributes:['shipping_company','shipping_key','mall_shipping_service']
    //     },{model:MarketOrderTime}]
    // }));
    //console.log("asdasd asd asd",JSON.stringify(Consumer_Order))

}
exports.lolisticCompleteOrder = lolisticCompleteOrder;





var nonLolisticCompleteOrder = async function () {

    console.log("run lolisticCompleteOrder")
    
    var t = 'YYYY-MM-DD HH:mm:ss';

    var s = moment().format(t);
    var e = moment().add(59, 's').format(t);

    var now = moment().format(t)
    var review_expired = moment().add(14, 'days').format(t)

    // s = '2019-04-18 16:08:00'
    // e = '2019-04-18 16:08:59'

    

    sequelize.query("select * from (SELECT  `ConsumerMarketOrder`.`order_id`, `ConsumerMarketOrder`.`consumer_id`, `ConsumerMarketOrder`.`status`,  `ConsumerMarketOrder`.`market_refund_id`, `ConsumerMarketOrder`.`order_date`,  `ConsumerMarketOrder`.`carrier_company`, `ConsumerMarketOrder`.`createdAt`, `ConsumerMarketOrder`.`updatedAt`, `MarketRefund`.`market_refund_id` AS `MarketRefund.market_refund_id`, `MarketRefund`.`status` AS `MarketRefund.status`, `ShippingService`.`shipping_company`, `ShippingService`.`shipping_key`, `ShippingService`.`mall_shipping_service` ,`ShippingService`.`auto_complete_day`, `ConsumerMarketOrderTime`.`time_id` AS `ConsumerMarketOrderTime.time_id`, `ConsumerMarketOrderTime`.`order_id` AS `ConsumerMarketOrderTime.order_id`, `ConsumerMarketOrderTime`.`place_order_time` AS `ConsumerMarketOrderTime.place_order_time`, `ConsumerMarketOrderTime`.`payment_time` AS `ConsumerMarketOrderTime.payment_time`, `ConsumerMarketOrderTime`.`ship_time` AS `ConsumerMarketOrderTime.ship_time`, `ConsumerMarketOrderTime`.`estimated_time` AS `ConsumerMarketOrderTime.estimated_time`, `ConsumerMarketOrderTime`.`awaiting_delivery_time` AS `ConsumerMarketOrderTime.awaiting_delivery_time`, `ConsumerMarketOrderTime`.`in_delivery_time` AS `ConsumerMarketOrderTime.in_delivery_time`, `ConsumerMarketOrderTime`.`ship_time` + interval `ShippingService`.`auto_complete_day` DAY as auto_complete_date,`MarketRefund`.`consumer_cancel_time`  + interval `ShippingService`.`auto_complete_day` DAY as con_cxl_delay_complete_date,`MarketRefund`.`admin_cancel_time`  + interval `ShippingService`.`auto_complete_day` DAY as admin_cxl_delay_complete_date,`MarketRefund`.`admin_complete_time`  + interval `ShippingService`.`auto_complete_day` DAY as admin_delay_complete_date, `ConsumerMarketOrderTime`.`order_receive_time` AS `ConsumerMarketOrderTime.order_receive_time`, `ConsumerMarketOrderTime`.`cancel_time` AS `ConsumerMarketOrderTime.cancel_time`, `ConsumerMarketOrderTime`.`refund_time` AS `ConsumerMarketOrderTime.refund_time`, `ConsumerMarketOrderTime`.`complete_time` AS `ConsumerMarketOrderTime.complete_time` FROM `market_order` AS `ConsumerMarketOrder` LEFT OUTER JOIN `market_refund` AS `MarketRefund` ON `ConsumerMarketOrder`.`market_refund_id` = `MarketRefund`.`market_refund_id` INNER JOIN `shipping_service` AS `ShippingService` ON `ConsumerMarketOrder`.`carrier_company` = `ShippingService`.`shipping_key` AND `ShippingService`.`mall_shipping_service` = 0 INNER JOIN `market_order_time` AS `ConsumerMarketOrderTime` ON `ConsumerMarketOrder`.`order_id` = `ConsumerMarketOrderTime`.`order_id` AND `ConsumerMarketOrderTime`.`ship_time` is not null  WHERE  `ConsumerMarketOrder`.`status` = 2 ) as innertable where (auto_complete_date BETWEEN '"+s+"' AND '"+e+"' AND market_refund_id is null ) or ( `MarketRefund.status` > 5 AND con_cxl_delay_complete_date BETWEEN '"+s+"' AND '"+e+"' or admin_cxl_delay_complete_date BETWEEN '"+s+"' AND '"+e+"' or admin_delay_complete_date BETWEEN '"+s+"' AND '"+e+"')", {
            type: sequelize.QueryTypes.SELECT
        }).then(orderList => {




            
        orderList.forEach(async function (orderDetail) {
            if(orderDetail['MarketRefund.status']>5||orderDetail['MarketRefund.status']==null)
            {  
                 console.log("asda sd a sd ",JSON.stringify(orderDetail))
                console.log("refund status",orderDetail['MarketRefund.status'])

                 var err, Update_Consumer_Order
                [err, Update_Consumer_Order] = await to( MarketOrder.update({
                        status: 3,
                        complete_time:now,
                        product_review_expire_time:review_expired,
                        completed_by_system:1
                    }, {
                        where: {
                            order_id: orderDetail.order_id
                        }
                    })
                );

                console.log("err",JSON.stringify(err))
                console.log("Update_Consumer_Order",JSON.stringify(Update_Consumer_Order))

               var Update_MarketOrder_Time
                [err, Update_MarketOrder_Time] = await to( MarketOrderTime.update({
                        complete_time:now
                    }, {
                        where: {
                            order_id: orderDetail.order_id
                        }
                    })
                );

                console.log("err",JSON.stringify(err))
                console.log("Update_MarketOrder_Time",JSON.stringify(Update_MarketOrder_Time))

                var rebate_transaction = await helper.rebateTransaction(orderDetail.order_id)

                let msg = `Your Order #${orderDetail.order_id} has been completed`
            
                var notiData = {}
                notiData.order_id = orderDetail.order_id
                notiData.status = 3

                pushNotification.handleNotification(true, orderDetail.consumer_id, orderDetail.order_id, "consumer", "mall_order", msg, null, "Complete Order", true, "order_completed", JSON.stringify(notiData))

            }
            else
            {
                console.log("refund processing or refund complete")
            }

    
       })



        }).catch(err => {
            console.log(err);
        })
}
exports.nonLolisticCompleteOrder = nonLolisticCompleteOrder;


var nonLolisticRemindComplete = async function () {

    
    console.log("non lolistic remind auto complete")
    
    var t = 'YYYY-MM-DD HH:mm:ss';

    var s = moment().format(t);
    var e = moment().add(59, 's').format(t);

    var now = moment().format(t)

    var tmr = moment().add(1, 'days').format(t)
    // s = '2019-04-17 16:08:00'
    // e = '2019-04-17 16:08:59'


    sequelize.query("select * from (SELECT  `ConsumerMarketOrder`.`order_id`, `ConsumerMarketOrder`.`consumer_id`, `ConsumerMarketOrder`.`status`,  `ConsumerMarketOrder`.`market_refund_id`, `ConsumerMarketOrder`.`order_date`,  `ConsumerMarketOrder`.`carrier_company`, `ConsumerMarketOrder`.`createdAt`, `ConsumerMarketOrder`.`updatedAt`, `MarketRefund`.`market_refund_id` AS `MarketRefund.market_refund_id`, `MarketRefund`.`status` AS `MarketRefund.status`, `ShippingService`.`shipping_company`, `ShippingService`.`shipping_key`, `ShippingService`.`mall_shipping_service` ,`ShippingService`.`auto_complete_day`, `ConsumerMarketOrderTime`.`time_id` AS `ConsumerMarketOrderTime.time_id`, `ConsumerMarketOrderTime`.`order_id` AS `ConsumerMarketOrderTime.order_id`, `ConsumerMarketOrderTime`.`place_order_time` AS `ConsumerMarketOrderTime.place_order_time`, `ConsumerMarketOrderTime`.`payment_time` AS `ConsumerMarketOrderTime.payment_time`, `ConsumerMarketOrderTime`.`ship_time` AS `ConsumerMarketOrderTime.ship_time`, `ConsumerMarketOrderTime`.`estimated_time` AS `ConsumerMarketOrderTime.estimated_time`, `ConsumerMarketOrderTime`.`awaiting_delivery_time` AS `ConsumerMarketOrderTime.awaiting_delivery_time`, `ConsumerMarketOrderTime`.`in_delivery_time` AS `ConsumerMarketOrderTime.in_delivery_time`, `ConsumerMarketOrderTime`.`ship_time` + interval `ShippingService`.`auto_complete_day` -1 DAY as auto_complete_date,`MarketRefund`.`consumer_cancel_time`  + interval `ShippingService`.`auto_complete_day`-1 DAY as con_cxl_delay_complete_date,`MarketRefund`.`admin_cancel_time`  + interval `ShippingService`.`auto_complete_day`-1 DAY as admin_cxl_delay_complete_date,`MarketRefund`.`admin_complete_time`  + interval `ShippingService`.`auto_complete_day` -1 DAY as admin_delay_complete_date, `ConsumerMarketOrderTime`.`order_receive_time` AS `ConsumerMarketOrderTime.order_receive_time`, `ConsumerMarketOrderTime`.`cancel_time` AS `ConsumerMarketOrderTime.cancel_time`, `ConsumerMarketOrderTime`.`refund_time` AS `ConsumerMarketOrderTime.refund_time`, `ConsumerMarketOrderTime`.`complete_time` AS `ConsumerMarketOrderTime.complete_time` FROM `market_order` AS `ConsumerMarketOrder` LEFT OUTER JOIN `market_refund` AS `MarketRefund` ON `ConsumerMarketOrder`.`market_refund_id` = `MarketRefund`.`market_refund_id` INNER JOIN `shipping_service` AS `ShippingService` ON `ConsumerMarketOrder`.`carrier_company` = `ShippingService`.`shipping_key` AND `ShippingService`.`mall_shipping_service` = 0 INNER JOIN `market_order_time` AS `ConsumerMarketOrderTime` ON `ConsumerMarketOrder`.`order_id` = `ConsumerMarketOrderTime`.`order_id` AND `ConsumerMarketOrderTime`.`ship_time` is not null  WHERE  `ConsumerMarketOrder`.`status` = 2 ) as innertable where (auto_complete_date BETWEEN '"+s+"' AND '"+e+"' AND market_refund_id is null ) or ( `MarketRefund.status` > 5 AND con_cxl_delay_complete_date BETWEEN '"+s+"' AND '"+e+"' or admin_cxl_delay_complete_date BETWEEN '"+s+"' AND '"+e+"' or admin_delay_complete_date BETWEEN '"+s+"' AND '"+e+"')", {
        type: sequelize.QueryTypes.SELECT
    }).then(orderList => {

        //console.log("asd asd asd ad "+JSON.stringify(orderList))

                 
        orderList.forEach(async function (orderDetail) {
            if(orderDetail['MarketRefund.status']>5||orderDetail['MarketRefund.status']==null)
            {  

       

                 let msg = `Your Order #${orderDetail.order_id} will auto complete at `+tmr
            
                var notiData = {}
                notiData.order_id = orderDetail.order_id
                notiData.status = 2

                pushNotification.handleNotification(true, orderDetail.consumer_id, orderDetail.order_id, "consumer", "mall_order", msg, null, "Complete Order", true, "order_remind", JSON.stringify(notiData))

            }
            else
            {
                console.log("refund processing or refund complete")
            }

    
       })




    }).catch(err => {
        console.log(err);
    })


}
exports.nonLolisticRemindComplete = nonLolisticRemindComplete;

