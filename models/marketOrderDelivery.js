'use strict';

const moment = require('moment');
module.exports = (sequelize, DataTypes) => {
  var ConsumerMarketOrderDelivery = sequelize.define('ConsumerMarketOrderDelivery', {
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      primaryKey: true,
      autoIncrement: true
    },
    order_id: DataTypes.INTEGER,
    others: DataTypes.STRING,
    rider_detail: DataTypes.STRING,
    order_rider_uuid: DataTypes.INTEGER.UNSIGNED,
    estimated_time: 'TIMESTAMP'


  }, {
    freezeTableName: true,
    tableName: 'market_order_delivery',
    timestamps: false,
    getterMethods: {  estimated_time: function () {
      if(this.getDataValue('estimated_time'))
      {
          return moment(this.getDataValue('estimated_time'), 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');}
      else if(this.getDataValue('estimated_time') === undefined)
          return ;
      else 
        return null;
    }
  }

  });

  ConsumerMarketOrderDelivery.associate = function (models) {
    // associations can be defined here

    // this.package_id = this.hasMany(models.ConsumerMarketOrder, {
    //   foreignKey: 'package_id'
    // });

  };
  return ConsumerMarketOrderDelivery;
};