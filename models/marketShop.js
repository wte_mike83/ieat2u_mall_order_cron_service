const moment = require('moment');
'use strict';

module.exports = (sequelize, DataTypes) => {

  var MarketShop = sequelize.define('MarketShop', {   
    
    shop_id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
    consumer_id: DataTypes.INTEGER,
    url_name: DataTypes.STRING,
    shop_name: DataTypes.STRING,
    shop_type: DataTypes.STRING,
    profile_pic: DataTypes.STRING,
    profile_thumbnail: DataTypes.STRING,
    profile_checksum: DataTypes.STRING,
    total_vote: DataTypes.INTEGER,
    total_rate: DataTypes.INTEGER,
    total_product: DataTypes.INTEGER,
    published_product: DataTypes.INTEGER,
    shipping_service: DataTypes.STRING,
    description_image: DataTypes.STRING,
    shop_description: DataTypes.STRING,
    //logout_time: 'TIMESTAMP',
    //contact_country_code: DataTypes.STRING,
    //shop_contact_no: DataTypes.STRING,
    is_verify: DataTypes.TINYINT,    
    //acc_active: DataTypes.TINYINT,
    charge_rate: DataTypes.DECIMAL,
    update_shop_name_tms:'TIMESTAMP',
    lolol_mall:DataTypes.TINYINT,
    shop_latitude: DataTypes.DECIMAL,
    shop_longitude: DataTypes.DECIMAL,
    shop_address:DataTypes.STRING,
    min_order: DataTypes.DECIMAL,

    createdAt:'TIMESTAMP',
    updatedAt: 'TIMESTAMP'
  
  }, {
    freezeTableName: true,
    tableName: 'market_shop',
    timestamps: true, 
    getterMethods: {

      profile_pic: function () {

        if(this.getDataValue('profile_pic'))
        {
          return `${CONFIG.filePath}/images/shop/${this.getDataValue('shop_id')}/profile/${this.getDataValue('profile_pic')}`;
        }
  
      },
      profile_thumbnail: function () {

        if(this.getDataValue('profile_thumbnail'))
        {
          return `${CONFIG.filePath}/images/shop/${this.getDataValue('shop_id')}/profile/${this.getDataValue('profile_thumbnail')}`;
        }
   
      },
      createdAt: function () {
        if(this.getDataValue('createdAt'))
        {
            return moment(this.getDataValue('createdAt'), 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');
        }
        else if(this.getDataValue('createdAt') === undefined)
            return ;
        else 
          return null;
      },
      // description_image:  function() {

      //   if(this.getDataValue('description_image') != null)
      //   {
          
      //      var pic = JSON.parse(this.getDataValue('description_image'))

      //     for ( i = 0; i < pic.length; i++) {

      //       pic[i].description_image = CONFIG.filePath +"/images/shop/"+this.getDataValue('shop_id') +"/description/"+pic[i].description_image;
      //       pic[i].thumbnail = CONFIG.filePath +"/images/shop/"+this.getDataValue('shop_id') +"/description/"+pic[i].thumbnail;
      //       pic[i].thumbnail_square = CONFIG.filePath +"/images/shop/"+this.getDataValue('shop_id') +"/description/"+pic[i].thumbnail_square;

      //     }
      //     return pic;
      //   }
      //   return [];

      // },
      // status: function() {

      //   if(this.getDataValue('logout_time') == "Invalid Date") {
      //     status = "Active";
      //   }else {
      //     status = "Active " + moment(this.getDataValue('logout_time')).fromNow();
      //   }
      //   return status;
      // }, 
      // rating: function() {

      //   var rating = (this.getDataValue('total_rate')/this.getDataValue('total_vote')).Fixed(2);

      //   if(isNaN(rating)) {
      //     rating = 0;
      //   }

      //   return rating;
      // },
      // joined: function() {
      //   return moment(this.getDataValue('createdAt')).fromNow();
      // },
      // shipping_service: function() {

      //   if(this.getDataValue('shipping_service')) {

      //   }
      //   console.log(JSON.parse(this.getDataValue('shipping_service')))
      // }
    } 
  });

  return MarketShop;
};