const moment = require('moment');


'use strict';
module.exports = (sequelize, DataTypes) => {
  var ConsumerMarketOrder = sequelize.define('ConsumerMarketOrder', {
    order_id: {
      type: DataTypes.INTEGER.UNSIGNED,
      primaryKey: true,
      autoIncrement: true
    },
    package_id: DataTypes.INTEGER.UNSIGNED,
    consumer_id: DataTypes.INTEGER.UNSIGNED,
    fullname: DataTypes.STRING,
    contact: DataTypes.STRING,
    shop_id: DataTypes.INTEGER.UNSIGNED,
    status: DataTypes.INTEGER.UNSIGNED,
    market_cancel_id: DataTypes.INTEGER,
    market_refund_id:DataTypes.INTEGER.UNSIGNED,
    //product_invalid: DataTypes.INTEGER,
    order_date: 'TIMESTAMP',
    item: {
      type: DataTypes.STRING,
      allowNull: true,
      get: function () {
        if(this.getDataValue('item'))
        return JSON.parse(this.getDataValue('item'));
      },
      set: function (item) {
        if(item)
        this.setDataValue('item', JSON.stringify(item));

      }
    },
    weight: DataTypes.STRING,
    total_payment: DataTypes.STRING,
    sub_total: DataTypes.STRING,
    voucher_amount: DataTypes.STRING,
    discount_amount: DataTypes.STRING,
    consumer_address:DataTypes.STRING,
    //delivery_address: DataTypes.STRING,
    carrier_company: DataTypes.STRING,
    tracking_no: DataTypes.STRING,
    total_shipping_fee: DataTypes.STRING,
    ship_warning:{
      type: DataTypes.STRING,
      allowNull: true,
      get: function () {
        if(this.getDataValue('ship_warning'))
        return JSON.parse(this.getDataValue('ship_warning'));
      },
      set: function (item) {
        if(item)
        this.setDataValue('ship_warning', JSON.stringify(item));

      }
    },
    //shipping_invalid: DataTypes.INTEGER,
    ship_time: 'TIMESTAMP',
    payment_time: 'TIMESTAMP',
    complete_time: 'TIMESTAMP',
    reject_type: DataTypes.STRING,
    cancel_time: 'TIMESTAMP',
    refund_time: 'TIMESTAMP',
    prefer_time: 'TIMESTAMP',
    remark: DataTypes.STRING,
    reason: DataTypes.STRING,
    extra: DataTypes.STRING,
    voucher_list: DataTypes.STRING,
    secret_code: DataTypes.STRING,
    refer_order_id: DataTypes.STRING,
    product_review_expire_time: 'TIMESTAMP',
    completed_by_system: DataTypes.INTEGER.UNSIGNED,

  }, {
    freezeTableName: true,
    tableName: 'market_order',
    timestamps: true,
    getterMethods: {
       createdAt: function () {
        if(this.getDataValue('createdAt'))
        {
            return moment(this.getDataValue('createdAt'), 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');}
        else if(this.getDataValue('createdAt') === undefined)
            return ;
        else 
          return null;
      },
      payment_time: function () {
        if(this.getDataValue('payment_time'))
        {
            return moment(this.getDataValue('payment_time'), 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');}
        else if(this.getDataValue('payment_time') === undefined)
            return ;
        else 
          return null;
      },
      updatedAt: function () {
        if(this.getDataValue('updatedAt'))
        {
            return moment(this.getDataValue('updatedAt'), 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');}
        else if(this.getDataValue('updatedAt') === undefined)
            return ;
        else 
          return null;
      },
      order_date: function () {
        if(this.getDataValue('order_date'))
        {
            return moment(this.getDataValue('order_date'), 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');}
        else if(this.getDataValue('order_date') === undefined)
            return ;
        else 
          return null;
      },
      ship_time: function () {
        if(this.getDataValue('ship_time'))
        {
            return moment(this.getDataValue('ship_time'), 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');}
        else if(this.getDataValue('ship_time') === undefined)
            return ;
        else 
          return null;
      },
      payment_time: function () {
        if(this.getDataValue('payment_time'))
        {
            return moment(this.getDataValue('payment_time'), 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');}
        else if(this.getDataValue('payment_time') === undefined)
            return ;
        else 
          return null;
      },
      complete_time: function () {
        if(this.getDataValue('complete_time'))
        {
            return moment(this.getDataValue('complete_time'), 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');}
        else if(this.getDataValue('complete_time') === undefined)
            return ;
        else 
          return null;
      },
      cancel_time: function () {
        if(this.getDataValue('cancel_time'))
        {
            return moment(this.getDataValue('cancel_time'), 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');}
        else if(this.getDataValue('cancel_time') === undefined)
            return ;
        else 
          return null;
      },
      refund_time: function () {
        if(this.getDataValue('refund_time'))
        {
            return moment(this.getDataValue('refund_time'), 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');}
        else if(this.getDataValue('refund_time') === undefined)
            return ;
        else 
          return null;
      },
      prefer_time: function () {
        if(this.getDataValue('prefer_time'))
        {
            return moment(this.getDataValue('prefer_time'), 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');}
        else if(this.getDataValue('prefer_time') === undefined)
            return ;
        else 
          return null;
      },
      status_text: function () {
        var stat = {
          0: "PENDING",
          1: "TO SHIP",
          2: "SHIPPING",
          3: "COMPLETED",
          4: "CANCELLED",
          5: "REFUNDED",
         // 6: "RETURN/REFUND"

        };

        return stat[this.status];
      }

    }

  });
  



  ConsumerMarketOrder.associate = function (models) {
    // associations can be defined here
    this.consumer_id = this.belongsTo(models.Consumer, {
      foreignKey: 'consumer_id'
    });
    
    this.shop_id = this.belongsTo(models.MarketShop, {
      foreignKey: 'shop_id'
    });

    this.market_refund_id = this.belongsTo(models.MarketRefund, {
      foreignKey: 'market_refund_id'
    });

    this.market_cancel_id = this.belongsTo(models.MarketCancel, {
      foreignKey: 'market_cancel_id'
    });
 

    this.ConsumerMarketOrderDelivery = this.hasOne(models.ConsumerMarketOrderDelivery, {
      foreignKey: 'order_id'
    });
 
    this.ConsumerMarketOrderTime = this.hasOne(models.ConsumerMarketOrderTime, {
      foreignKey: 'order_id'
    });
 





    this.belongsTo(models.ShippingService, {foreignKey: 'carrier_company', targetKey: 'shipping_key'}); 


  };
  return ConsumerMarketOrder;
};