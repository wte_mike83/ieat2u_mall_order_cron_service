'use strict';
module.exports = (sequelize, DataTypes) => {
  var ConsumerMarketCart = sequelize.define('ConsumerMarketCart', {
    cart_id               : {type:DataTypes.INTEGER.UNSIGNED ,primaryKey: true,autoIncrement: true},
    consumer_id				    : DataTypes.INTEGER.UNSIGNED,
    device_id             : DataTypes.INTEGER.UNSIGNED,
    shop_id         : DataTypes.INTEGER.UNSIGNED,
    product_id               : DataTypes.INTEGER.UNSIGNED,
    quantity              : DataTypes.INTEGER.UNSIGNED,
    price                 : DataTypes.STRING,
    saved_price           : DataTypes.STRING,
    item_variations           : DataTypes.STRING,
    variations_error    : DataTypes.INTEGER,
    item_update         : DataTypes.STRING,
    invalid               : DataTypes.BOOLEAN,
    invalid_reason            : DataTypes.STRING,
    
  }, {
    freezeTableName: true,
    tableName: 'market_cart',

  });
  ConsumerMarketCart.associate = function(models) {
    // associations can be defined here
    this.consumer_id = this.hasMany(models.Consumer, {foreignKey: 'consumer_id'});
    this.shop_id = this.belongsTo(models.MarketShop, {foreignKey: 'shop_id'});
    this.product_id = this.belongsTo(models.MarketProduct, {foreignKey: 'product_id'});
  };
  return ConsumerMarketCart;
};