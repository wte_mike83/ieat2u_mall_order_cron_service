'use strict';
module.exports = (sequelize, DataTypes) => {
  var ConsumerMarketOrderPackage = sequelize.define('ConsumerMarketOrderPackage', {
    package_id: {
      type: DataTypes.INTEGER.UNSIGNED,
      primaryKey: true,
      autoIncrement: true
    },
    consumer_id: DataTypes.INTEGER,
    total_payment: DataTypes.STRING,
    payment_method: DataTypes.STRING,
    number_item: DataTypes.INTEGER.UNSIGNED,


  }, {
    freezeTableName: true,
    tableName: 'market_order_package',
    timestamps: true,


  });

  ConsumerMarketOrderPackage.associate = function (models) {
    // associations can be defined here

    this.package_id = this.hasMany(models.ConsumerMarketOrder, {
      foreignKey: 'package_id'
    });

  };
  return ConsumerMarketOrderPackage;
};