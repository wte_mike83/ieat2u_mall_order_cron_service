const moment = require('moment');
'use strict';

module.exports = (sequelize, DataTypes) => {
   
  var MarketRefund = sequelize.define('MarketRefund', {
    market_refund_id    : {type:DataTypes.INTEGER.UNSIGNED ,primaryKey: true,autoIncrement: true},
    order_id            : DataTypes.INTEGER.UNSIGNED,
    shop_id             : DataTypes.INTEGER.UNSIGNED,
    consumer_id         : DataTypes.INTEGER.UNSIGNED,
    type   		          : DataTypes.TINYINT,
    amount   		        : DataTypes.DECIMAL,
    reason 	  	        : DataTypes.TINYINT,               
    explanation     		: DataTypes.STRING,
    status 		          : DataTypes.TINYINT,
    refund_image        : {type: DataTypes.STRING, allowNull: true},
    is_return           : DataTypes.TINYINT,
    admin_process_time  : 'TIMESTAMP',
    admin_accept_description: DataTypes.STRING,
    admin_accept_time   : 'TIMESTAMP',
    seller_process_time : 'TIMESTAMP',
    consumer_cancel_time: 'TIMESTAMP',
    admin_cancel_reason: DataTypes.STRING,
    admin_cancel_description: DataTypes.STRING,
    admin_cancel_time   : 'TIMESTAMP',
    seller_cancel_description: DataTypes.STRING,
    seller_cancel_time  : 'TIMESTAMP',
    admin_complete_description: DataTypes.STRING,
    admin_complete_time : 'TIMESTAMP',
    seller_complete_description: DataTypes.STRING,
    seller_complete_time: 'TIMESTAMP',

  }, {
    freezeTableName: true,
    tableName: 'market_refund',

    timestamps: false,

    getterMethods: {

      refund_image:  function() {

        if(this.getDataValue('refund_image') != null)
        {
          var pic = JSON.parse(this.getDataValue('refund_image'))

          for (var i = 0; i < pic.length; i++) {

            pic[i].refund_image = CONFIG.filePath +"/marketRefund/"+this.getDataValue('order_id') +"/"+ pic[i].refund_image;
            pic[i].thumbnail = CONFIG.filePath +"/marketRefund/"+this.getDataValue('order_id') +"/"+ pic[i].thumbnail;
            pic[i].thumbnail_square = CONFIG.filePath +"/marketRefund/"+this.getDataValue('order_id') +"/"+ pic[i].thumbnail_square;

          }
          return pic;
        }
        return [];

      },
      admin_process_time: function() {
        if(this.getDataValue('admin_process_time') != null) {
          return(moment(this.getDataValue('admin_process_time')).format("YYYY-MM-DD HH:mm:ss"));
        }
        return this.getDataValue('admin_process_time');
      },
      admin_accept_time: function() {
        if(this.getDataValue('admin_accept_time') != null) {
          return(moment(this.getDataValue('admin_accept_time')).format("YYYY-MM-DD HH:mm:ss"));
        }
        return this.getDataValue('admin_accept_time');
      },
      seller_process_time: function() {
        if(this.getDataValue('seller_process_time') != null) {
          return(moment(this.getDataValue('seller_process_time')).format("YYYY-MM-DD HH:mm:ss"));
        }
        return this.getDataValue('seller_process_time');
      },
      consumer_cancel_time: function() {
        if(this.getDataValue('consumer_cancel_time') != null) {
          return(moment(this.getDataValue('consumer_cancel_time')).format("YYYY-MM-DD HH:mm:ss"));
        }
        return this.getDataValue('admin_cancel_time');
      },
      admin_cancel_time: function() {
        if(this.getDataValue('admin_cancel_time') != null) {
          return(moment(this.getDataValue('admin_cancel_time')).format("YYYY-MM-DD HH:mm:ss"));
        }
        return this.getDataValue('admin_cancel_time');
      },
      seller_cancel_time: function() {
        if(this.getDataValue('seller_cancel_time') != null) {
          return(moment(this.getDataValue('seller_cancel_time')).format("YYYY-MM-DD HH:mm:ss"));
        }
        return this.getDataValue('seller_cancel_time');
      },
      admin_complete_time: function() {
        if(this.getDataValue('admin_complete_time') != null) {
          return(moment(this.getDataValue('admin_complete_time')).format("YYYY-MM-DD HH:mm:ss"));
        }
        return this.getDataValue('admin_complete_time');
      },
      seller_complete_time: function() {
        if(this.getDataValue('seller_complete_time') != null) {
          return(moment(this.getDataValue('seller_complete_time')).format("YYYY-MM-DD HH:mm:ss"));
        }
        return this.getDataValue('seller_complete_time');
      },
      status_text: function() {
          var stat = {
            1: "SELLER PENDING",
            2: "SELLER COMPLETED",
            3: "ADMIN PENDING",
            4: "ADMIN ACCEPTED",
            5: "ADMIN COMPLETED",
            6: "CANCELLED BY CONSUMER",
            7: "CANCELLED BY SELLER",
            8: "CANCELLED BY ADMIN",
  
          };
  
          return stat[this.status];
      },
      reason_text:  function() {

        var reason_txt = {
          0: "Others",
          1: "Did not receive the order",
          2: "Received incomplete product(s) (Missing quantity or accessories)",
          3: "Received wrong product(s) (Wrong size, wrong colour, different product etc.)",
          4: "Received product(s) with physical damage (Dented, scratched, shattered etc.)",
        }

        return reason_txt[this.reason];

      }

    } 

  });

  MarketRefund.associate = function(models) {
    // this.order_id = this.belongsTo(models.ConsumerMarketOrder, {foreignKey: 'order_id'});
    // this.shop_id = this.belongsTo(models.MarketShop, {foreignKey: 'shop_id'});
    // this.order_id = this.belongsTo(models.MarketRefund, {foreignKey: 'order_id'});
  };
  
  MarketRefund.prototype.toWeb = function (pw) {
      let json = this.toJSON();
      return json;
  };

  return MarketRefund;
};