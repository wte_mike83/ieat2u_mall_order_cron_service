'use strict';
module.exports = (sequelize, DataTypes) => {
  var Holiday = sequelize.define('Holiday', {

    holiday_id: {type:DataTypes.INTEGER ,primaryKey: true,autoIncrement: true} ,
    holiday_date: DataTypes.STRING,
    holiday_name: DataTypes.STRING,
    holiday_country: DataTypes.STRING,
    holiday_state:DataTypes.STRING,
    year_flag:DataTypes.BOOLEAN,

  }, {
    freezeTableName: true,
    tableName: 'holiday',
  });

  Holiday.associate = function(models) {
    
  };

  return Holiday;
};