'use strict';
const moment = require('moment'); 
module.exports = (sequelize, DataTypes) => {
  var Transaction = sequelize.define('Transaction', {

    trans_id: {type:DataTypes.INTEGER ,primaryKey: true,autoIncrement: true} ,
    consumer_id: DataTypes.INTEGER.UNSIGNED,
    order_id: DataTypes.INTEGER.UNSIGNED,
    order_type: DataTypes.INTEGER.UNSIGNED,
    trans_type:DataTypes.STRING,
    trans_status:DataTypes.STRING,
    trans_amount:DataTypes.DECIMAL,
    trans_no:DataTypes.STRING,
    trans_method:DataTypes.STRING,
    trans_date:'TIMESTAMP',
    trans_update_date:'TIMESTAMP',
    remark:DataTypes.STRING,
    refer:DataTypes.INTEGER,
    trans_extra:DataTypes.TEXT,
    batch_time:DataTypes.DATE,

  }, {
    freezeTableName: true,
    tableName: 'consumer_transaction',
    createdAt: 'trans_date',
    updatedAt: 'trans_update_date',
    getterMethods: {
      date:function(){
          return moment(this.trans_date).format('Y/MM/DD');
      },
      trans_date:function(){
        if(this.getDataValue('trans_date'))
          return moment(this.getDataValue('trans_date'), 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');
        else if(this.getDataValue('trans_date') === undefined)
            return ;
        else 
          return null;
      }
    }
  });

  Transaction.associate = function(models) {

    Transaction.belongsTo(models.Consumer, {
      foreignKey: 'consumer_id',
      as:'consumer'
    });

    // Transaction.belongsTo(models.MolpayDetails, {
    //   foreignKey: 'refer',
    //   targetKey:'payment_id',
    // });

    // Transaction.belongsTo(models.ConsumerOrder, {
    //   foreignKey: 'order_id',
    //   as:'order'
    // });
    
  };

  return Transaction;
};