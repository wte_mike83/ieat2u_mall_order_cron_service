'use strict';
module.exports = (sequelize, DataTypes) => {
  var District = sequelize.define('District', {
    postcode_id: {type:DataTypes.INTEGER.UNSIGNED ,primaryKey: true,autoIncrement: true} ,
    state_id: DataTypes.INTEGER.UNSIGNED,
    postcode: DataTypes.STRING,
    sub_district: DataTypes.STRING,

  }, {
    freezeTableName: true,
    tableName: 'state_postcode',
  });
  
  District.associate = function(models) {
    this.state_id = this.belongsTo(models.State, {foreignKey: 'state_id'});
  };
  
  return District;
};