'use strict';
const moment = require('moment');
module.exports = (sequelize, DataTypes) => {
  var ConsumerMarketOrderTime = sequelize.define('ConsumerMarketOrderTime', {
    time_id: {
      type: DataTypes.INTEGER.UNSIGNED,
      primaryKey: true,
      autoIncrement: true
    },
    order_id: DataTypes.INTEGER.UNSIGNED,
    consumer_id: DataTypes.INTEGER.UNSIGNED,

    place_order_time: 'TIMESTAMP',
    payment_time: 'TIMESTAMP',
    ship_time: 'TIMESTAMP',
    estimated_time: 'TIMESTAMP',
    //going_to_merchant_time: 'TIMESTAMP',
    awaiting_delivery_time: 'TIMESTAMP',
    in_delivery_time: 'TIMESTAMP',
    order_receive_time: 'TIMESTAMP',
    cancel_time: 'TIMESTAMP',
    refund_time: 'TIMESTAMP',
    complete_time: 'TIMESTAMP',

  }, {
    freezeTableName: true,
    tableName: 'market_order_time',
    timestamps: false,  
    getterMethods: {
      place_order_time: function () {
        if(this.getDataValue('place_order_time'))
        {
            return moment(this.getDataValue('place_order_time'), 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');}
        else if(this.getDataValue('place_order_time') === undefined)
            return ;
        else 
          return null;
      },
      payment_time: function () {
      if(this.getDataValue('payment_time'))
      {
          return moment(this.getDataValue('payment_time'), 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');}
      else if(this.getDataValue('payment_time') === undefined)
          return ;
      else 
        return null;
    },
    ship_time: function () {
        if(this.getDataValue('ship_time'))
        {
            return moment(this.getDataValue('ship_time'), 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');}
        else if(this.getDataValue('ship_time') === undefined)
            return ;
        else 
          return null;
      },
       estimated_time: function () {
      if(this.getDataValue('estimated_time'))
      {
          return moment(this.getDataValue('estimated_time'), 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');}
      else if(this.getDataValue('estimated_time') === undefined)
          return ;
      else 
        return null;
    },  
      awaiting_delivery_time: function () {
      if(this.getDataValue('awaiting_delivery_time'))
      {
          return moment(this.getDataValue('awaiting_delivery_time'), 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');}
      else if(this.getDataValue('awaiting_delivery_time') === undefined)
          return ;
      else 
        return null;
    }, 
    in_delivery_time: function () {
      if(this.getDataValue('in_delivery_time'))
      {
          return moment(this.getDataValue('in_delivery_time'), 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');}
      else if(this.getDataValue('in_delivery_time') === undefined)
          return ;
      else 
        return null;
    },   
    cancel_time: function () {
      if(this.getDataValue('cancel_time'))
      {
          return moment(this.getDataValue('cancel_time'), 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');}
      else if(this.getDataValue('cancel_time') === undefined)
          return ;
      else 
        return null;
    },
    refund_time: function () {
        if(this.getDataValue('refund_time'))
        {
            return moment(this.getDataValue('refund_time'), 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');}
        else if(this.getDataValue('refund_time') === undefined)
            return ;
        else 
          return null;
      },
    order_receive_time: function () {
        if(this.getDataValue('order_receive_time'))
        {
            return moment(this.getDataValue('order_receive_time'), 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');}
        else if(this.getDataValue('order_receive_time') === undefined)
            return ;
        else 
          return null;
      },
    complete_time: function () {
      if(this.getDataValue('complete_time'))
      {
          return moment(this.getDataValue('complete_time'), 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');}
      else if(this.getDataValue('complete_time') === undefined)
          return ;
      else 
        return null;
    }
  
  }
  });

  ConsumerMarketOrderTime.associate = function (models) {
    // associations can be defined here

    // this.package_id = this.hasMany(models.ConsumerMarketOrder, {
    //   foreignKey: 'package_id'
    // });

  };
  return ConsumerMarketOrderTime;
};