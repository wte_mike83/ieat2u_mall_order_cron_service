'use strict';
module.exports = (sequelize, DataTypes) => {
  var ShippingService = sequelize.define('ShippingService', {
        service_id      : {type:DataTypes.INTEGER.UNSIGNED ,primaryKey: true,autoIncrement: true},
        shipping_company: DataTypes.STRING,
        shipping_key    : DataTypes.STRING,
        state           : DataTypes.STRING,
        mall_shipping_service: DataTypes.INTEGER,
        max_weight: DataTypes.STRING,
        published_product: DataTypes.STRING,
        available_postcode: DataTypes.STRING,
        createdAt       : 'TIMESTAMP',
        deletedAt       : 'TIMESTAMP',

    }, {
        freezeTableName: true,
        tableName: 'shipping_service',
        timestamps: false,
    });

    ShippingService.associate = function(models) {
  
       //this.state_id = this.hasMany(models.StatePostcode, {foreignKey: 'state_id'});
    };

    ShippingService.prototype.toWeb = function (pw) {
      let json = this.toJSON();
      return json;
  };






  return ShippingService;
};