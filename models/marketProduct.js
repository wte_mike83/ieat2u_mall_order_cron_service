const moment = require('moment');
'use strict';


module.exports = (sequelize, DataTypes) => {

  var MarketProduct = sequelize.define('MarketProduct', {
   
    
    product_id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
    shop_id: DataTypes.INTEGER,
    product_pic: DataTypes.STRING,
    product_name: DataTypes.STRING,
    sku_code: DataTypes.STRING,
    product_desc: DataTypes.STRING,
    price: DataTypes.STRING,
    min_price: DataTypes.DECIMAL,
    stock: DataTypes.INTEGER,
    variations: {type: DataTypes.STRING, 
      get: function () {
          if(this.getDataValue('variations'))
          
              return JSON.parse(this.getDataValue('variations'));
              
          else if(this.getDataValue('variations') === undefined)
              return ;
          else 
              return [];
      }},
    variations_setting: {type: DataTypes.STRING, 
      get: function () {
          if(this.getDataValue('variations_setting'))
          
              return JSON.parse(this.getDataValue('variations_setting'));
              
          else if(this.getDataValue('variations_setting') === undefined)
              return ;
          else 
              return [];
      }},
    category: DataTypes.STRING,
    other_detail: {type: DataTypes.STRING, 
      get: function () {
          if(this.getDataValue('other_detail'))
          
              return JSON.parse(this.getDataValue('other_detail'));
              
          else if(this.getDataValue('other_detail') === undefined)
              return ;
          else 
              return [];
      }},
    weight: DataTypes.DECIMAL,
    package_size: {type: DataTypes.STRING, 
      get: function () {
          if(this.getDataValue('package_size'))
          
              return JSON.parse(this.getDataValue('package_size'));
              
          else if(this.getDataValue('package_size') === undefined)
              return ;
          else 
              return [];
      }},
    condition: DataTypes.STRING,
    shipping_fee: {type: DataTypes.STRING, 
      get: function () {
          if(this.getDataValue('shipping_fee'))
          
              return JSON.parse(this.getDataValue('shipping_fee'));
              
          else if(this.getDataValue('shipping_fee') === undefined)
              return ;
          else 
              return [];
      }},
    publish: DataTypes.BOOLEAN,
    sold: DataTypes.INTEGER,
    verify: DataTypes.BOOLEAN,
    lolol_mall: DataTypes.BOOLEAN,
    createdAt:'TIMESTAMP',
  
  }, {
    freezeTableName: true,
    tableName: 'market_product',

    timestamps: true, 
    paranoid: true,

    getterMethods: {

      createdAt: function () {
        if(this.getDataValue('createdAt'))
        {
            return moment(this.getDataValue('createdAt'), 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');}
        else if(this.getDataValue('createdAt') === undefined)
            return ;
        else 
          return null;
       }, 
       product_pic:  function() {

        if(this.getDataValue('product_pic') != null)
        {
          
           var pic = JSON.parse(this.getDataValue('product_pic'))

           if(this.getDataValue('shop_id'))
           {
              for ( i = 0; i < pic.length; i++) {
                //console.log("actual"+pic[i].product_img)
                pic[i].product_img = CONFIG.filePath +"shop/"+this.getDataValue('shop_id') +"/product/"+pic[i].product_img;
                pic[i].thumbnail = CONFIG.filePath +"shop/"+this.getDataValue('shop_id') +"/product/"+pic[i].thumbnail;
                pic[i].thumbnail_square = CONFIG.filePath +"shop/"+this.getDataValue('shop_id') +"/product/"+pic[i].thumbnail_square;
                pic[i].checkSum; 
              }
           }
          
          return pic;
        }

       // else
          //return CONFIG.filePath +'profile/default-user.png';

      }
    } 
  });



  MarketProduct.associate = function(models) {
    this.shop_id = this.belongsTo(models.MarketShop, {foreignKey: 'shop_id'});

  };


  return MarketProduct;
};