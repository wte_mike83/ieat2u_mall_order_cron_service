'use strict';
module.exports = (sequelize, DataTypes) => {
  
  var ConsumerAddress = sequelize.define('ConsumerAddress', {

    consumer_address_id: {type:DataTypes.INTEGER ,primaryKey: true,autoIncrement: true} ,
    consumer_id: DataTypes.INTEGER.UNSIGNED,
    phone_no: DataTypes.INTEGER.UNSIGNED,
    country_code: DataTypes.STRING,
    cus_name: DataTypes.INTEGER.UNSIGNED,
    address_type:DataTypes.STRING,
    address:DataTypes.TEXT,
    city:DataTypes.STRING,
    state:DataTypes.STRING,
    postcode:DataTypes.INTEGER,
    country:DataTypes.STRING,
    building:DataTypes.STRING,
    company:DataTypes.STRING,
    valid:DataTypes.INTEGER,
    latitude:DataTypes.DECIMAL,
    longitude:DataTypes.DECIMAL,
    others:DataTypes.STRING,
    default_addr:DataTypes.INTEGER,


  },  {
    freezeTableName: true,
    tableName: 'consumer_address',
    timestamps: false,
    getterMethods: {

      
    }

  });


  
  ConsumerAddress.prototype.delivery = function (pw) {
    let json = this.toJSON();
    delete json['cus_name'];
    delete json['country_code'];
    delete json['phone_no'];

    return json;
  };





  ConsumerAddress.associate = function(models) {

    ConsumerAddress.belongsTo(models.Consumer, {
      foreignKey: 'consumer_id',
      as:'consumer'
    });
  };

  return ConsumerAddress;
};