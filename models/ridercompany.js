'use strict';

module.exports = (sequelize, DataTypes) => {
	var RiderCompany = sequelize.define('RiderCompany', {
		id 	    : {type:DataTypes.INTEGER.UNSIGNED ,primaryKey: true,autoIncrement: true},
		name    : DataTypes.STRING,
	    ws_key  : DataTypes.STRING,
	    ip      : DataTypes.STRING,
	}, {
		freezeTableName: true,
        tableName: 'rider_company',

        timestamps: true,
        paranoid: true,
    });
    
	RiderCompany.associate = function(models) {
	// associations can be defined here
	};

  	return RiderCompany;
};