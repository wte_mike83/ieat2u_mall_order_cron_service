const moment = require('moment');
'use strict';

module.exports = (sequelize, DataTypes) => {

  var MarketCancel = sequelize.define('MarketCancel', {
    market_cancel_id: {
      type: DataTypes.INTEGER.UNSIGNED,
      primaryKey: true,
      autoIncrement: true
    },
    order_id: DataTypes.INTEGER.UNSIGNED,
    shop_id: DataTypes.INTEGER.UNSIGNED,
    consumer_id: DataTypes.INTEGER.UNSIGNED,
    amount: DataTypes.DECIMAL,
    reason_type: DataTypes.TINYINT,
    // reason_text   		          : DataTypes.STRING,
    // reason_text 	  	        : { type:DataTypes.TINYINT, 
    //                       get: function () {
    //                             var reason = "Other";

    //                             if(this.getDataValue('reason') != null) {

    //                                 if(this.getDataValue('reason') == 0) {
    //                                     reason = "Spoiled Product";
    //                                 }
    //                                 else if (this.getDataValue('reason') == 1) {
    //                                     reason = "Missing Product";
    //                                 }            
    //                             }

    //                             return reason;                                
    //                           }},
    status: DataTypes.TINYINT,
    is_lolol_mall: DataTypes.TINYINT,
    process_time: 'TIMESTAMP',
    accept_time: 'TIMESTAMP',
    reject_cancel_time: 'TIMESTAMP',
    complete_time: 'TIMESTAMP',
    consumer_cancel_time: 'TIMESTAMP',
    admin_cancel_reason: DataTypes.STRING,
    admin_cancel_description: DataTypes.STRING,
    reject_cancel_description: DataTypes.STRING,
    complete_description: DataTypes.STRING,
    remark:DataTypes.STRING

  }, {
    freezeTableName: true,
    tableName: 'market_cancel',
    timestamps: true,

    getterMethods: {
      process_time: function () {
        if (this.getDataValue('process_time') != null) {
          return (moment(this.getDataValue('process_time')).format("YYYY-MM-DD HH:mm:ss"));
        }
        return this.getDataValue('process_time');
      },
      accept_time: function () {
        if (this.getDataValue('accept_time') != null) {
          return (moment(this.getDataValue('accept_time')).format("YYYY-MM-DD HH:mm:ss"));
        }
        return this.getDataValue('accept_time');
      },
      reject_cancel_time: function () {
        if (this.getDataValue('reject_cancel_time') != null) {
          return (moment(this.getDataValue('reject_cancel_time')).format("YYYY-MM-DD HH:mm:ss"));
        }
        return this.getDataValue('reject_cancel_time');
      },
      complete_time: function () {
        if (this.getDataValue('complete_time') != null) {
          return (moment(this.getDataValue('complete_time')).format("YYYY-MM-DD HH:mm:ss"));
        }
        return this.getDataValue('complete_time');
      },
      consumer_cancel_time: function () {
        if (this.getDataValue('consumer_cancel_time') != null) {
          return (moment(this.getDataValue('consumer_cancel_time')).format("YYYY-MM-DD HH:mm:ss"));
        }
        return this.getDataValue('consumer_cancel_time');
      },
      createdAt: function () {
        if (this.getDataValue('createdAt') != null) {
          return (moment(this.getDataValue('createdAt')).format("YYYY-MM-DD HH:mm:ss"));
        }
        return this.getDataValue('createdAt');
      },
      updatedAt: function () {
        if (this.getDataValue('updatedAt') != null) {
          return (moment(this.getDataValue('updatedAt')).format("YYYY-MM-DD HH:mm:ss"));
        }
        return this.getDataValue('updatedAt');
      },
      status_text: function () {

        var bySide = {
          0: "SELLER",
          1: "ADMIN",
        }
        
        var stat = {
          1: bySide[this.is_lolol_mall]+" PROCESSING",
          2: bySide[this.is_lolol_mall]+" COMPLETED",
          3: "REQUEST REJECTED BY " + bySide[this.is_lolol_mall],
          4: "REQUEST CANCELLED BY CONSUMER",
          5: "ORDER CANCELLED BY ADMIN"
        };

        return stat[this.status];
      },
      reason_text: function () {


        var stat = {
          0:"Others",
          1: "Seller is not responsive to my inquiries",
          2: "Modify existing order",
          3: "Changed mind",
          4: "Found cheaper price",          
        };


        var reason_type = this.getDataValue('reason_type')

        if (reason_type||reason_type == 0)
          return stat[this.reason_type];
        else
          return null


        
      },
      cancelled_by_text: function () {
        if (this.getDataValue('status') == 2) {
          return "This order has been cancelled by user";
        }else if (this.getDataValue('status') == 5)
        {
          return "This order has been cancelled by seller";
        }
      },
      cancelled_by: function () {
        if (this.getDataValue('status') == 2) {
          return 0;
        }else if (this.getDataValue('status') == 5)
        {
          return 1;
        }
      }

    }

  });

  MarketCancel.associate = function (models) {
     this.order_id = this.belongsTo(models.ConsumerMarketOrder, {foreignKey: 'order_id'});
     this.shop_id = this.belongsTo(models.MarketShop, {foreignKey: 'shop_id'});
     this.consumer_id = this.belongsTo(models.Consumer, {foreignKey: 'consumer_id'});
    // this.order_id = this.belongsTo(models.MarketRefund, {foreignKey: 'order_id'});
  };

  MarketCancel.prototype.toWeb = function (pw) {
    let json = this.toJSON();
    return json;
  };

  return MarketCancel;
};