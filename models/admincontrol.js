'use strict';
module.exports = (sequelize, DataTypes) => {
  var AdminControl = sequelize.define('AdminControl', {
    operation: {type:DataTypes.STRING ,primaryKey: true} ,
    value: DataTypes.STRING,
    remark: DataTypes.STRING,
    label: DataTypes.STRING,
    unit: DataTypes.STRING,
    class: DataTypes.STRING,

  }, {
    freezeTableName: true,
    tableName: 'admin_control',
  });
  
  AdminControl.associate = function(models) {
    // associations can be defined here
  };
  return AdminControl;
};