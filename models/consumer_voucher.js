'use strict';
var moment = require('moment');
module.exports = (sequelize, DataTypes) => {
  var ConsumerVoucher = sequelize.define('ConsumerVoucher', {
    voucher_id: {type:DataTypes.INTEGER.UNSIGNED ,primaryKey: true,autoIncrement: true},
    package_id: DataTypes.INTEGER.UNSIGNED,
    consumer_id: DataTypes.INTEGER.UNSIGNED,
    uuid: DataTypes.STRING,
    status: DataTypes.STRING,
    order_id: DataTypes.INTEGER.UNSIGNED,
    use_restaurant: DataTypes.INTEGER.UNSIGNED,
    c2m_id: DataTypes.INTEGER.UNSIGNED,
    mobile_id: DataTypes.INTEGER.UNSIGNED,
    buy_from:DataTypes.INTEGER.UNSIGNED,
    mall_order_id:DataTypes.INTEGER.UNSIGNED,
    event_hook:DataTypes.STRING,
    referral_id:DataTypes.INTEGER.UNSIGNED,
    rebate_amount:DataTypes.DECIMAL,
    use_time:'TIMESTAMP',
    payment_time:'TIMESTAMP',
    batch_time:'TIMESTAMP',
  }, {
    freezeTableName: true,
    tableName: 'consumer_voucher',
    getterMethods: {
      order_date: function () {
        if(this.getDataValue('use_time'))
            return moment(this.getDataValue('use_time'), 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');
        else if(this.getDataValue('use_time') === undefined)
            return ;
        else 
          return null;
      },
      createdAt: function () {
        if(this.getDataValue('createdAt'))
            return moment(this.getDataValue('createdAt'), 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');
        else if(this.getDataValue('createdAt') === undefined)
            return ;
        else 
          return null;
      },
      payment_time: function () {
        if(this.getDataValue('payment_time'))
            return moment(this.getDataValue('payment_time'), 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');
        else if(this.getDataValue('payment_time') === undefined)
            return ;
        else 
          return null;
      }

    }
  });
  
  ConsumerVoucher.addHook('beforeFind',function (options) {


    // if(options.attributes)
    // {
    //     if(Array.isArray(options.attributes))
    //     {
    //         options.attributes.push(voucher_apply)
    //         options.attributes.push('voucher_list')
    //     }

    // }else
    // {
    //     options.attributes ={};
    //     options.attributes.include = [];
    //     options.attributes.include.push(voucher_apply);
    // }

    if(!options.attributes)
    {
        options.attributes ={};
        options.attributes.exclude = ['ConsumerOrderId'];
    }

  });


  ConsumerVoucher.associate = function(models) {
    // associations can be defined here

    //  ConsumerVoucher.belongsTo(models.ConsumerOrder, {as:'ApplingVoucher'});
  //  ConsumerVoucher.belongsTo(models.ConsumerOrder, {foreignKey: 'order_id'});

    ConsumerVoucher.belongsTo(models.Consumer, {foreignKey: 'consumer_id'});

    this.package_id = this.belongsTo(models.SpecialPackage, {foreignKey: 'package_id'});
  };
  return ConsumerVoucher;
};