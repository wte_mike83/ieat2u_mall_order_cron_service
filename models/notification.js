const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const moment = require('moment'); 

// Create Schema
const NotificationSchema = new Schema({

    user_type: {
        type: String,
        enum: ["merchant", "rider", "consumer"], 
        required: true
    },
    user_id: {
        type: Number, 
        required: true
    },
    notification_type: {
        type: String,
        enum: ["order", "review",'reserve','payment','kot','mall_order'], 
        required: true
    },
    type_id: {
        type: Number,
        required: true
    },
    message: {
      type: String, required: true, max: 150
    },
    tag: {
      type: String, max: 50,
      default: null
    },
    read: {
        type: Number,
        enum: [0, 1],
        default: 0
    },
    date: {
      type: Date,
      default: Date.now
      //required: true,
      //default: function(){return new Date(moment().utcOffset("+08:00").format('YYYY-MM-DD HH:mm:ss'))} 
    }
  });
  
  module.exports = NotificationModel = mongoose.model('notification', NotificationSchema);