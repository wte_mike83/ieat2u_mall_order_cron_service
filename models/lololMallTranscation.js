const moment = require('moment');
'use strict';

module.exports = (sequelize, DataTypes) => {

  var MarketShop = sequelize.define('LololMallTransaction', {   
    
    trans_id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
    order_id: DataTypes.INTEGER,
    shop_id: DataTypes.INTEGER,
    consumer_id: DataTypes.INTEGER,
    trans_type: DataTypes.STRING,
    trans_method: DataTypes.STRING,
    trans_status: DataTypes.STRING,
    trans_amount: DataTypes.STRING,
    trans_no: DataTypes.STRING,
    remark: DataTypes.STRING,
    refer: DataTypes.STRING,
    extra: DataTypes.STRING,

    createdAt:'TIMESTAMP',
    updatedAt: 'TIMESTAMP'
  
  }, {
    freezeTableName: true,
    tableName: 'lolol_mall_transaction',
    timestamps: true, 
  
  });

  return MarketShop;
};