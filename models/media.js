'use strict';
module.exports = (sequelize, DataTypes) => {
  
  var Media = sequelize.define('Media', {

    media_id: {type:DataTypes.INTEGER ,primaryKey: true,autoIncrement: true} ,
    restaurant_id   : DataTypes.INTEGER.UNSIGNED,
    merchant_id   : DataTypes.INTEGER.UNSIGNED,
    specialPackage_id   : DataTypes.INTEGER.UNSIGNED,
    filename:DataTypes.STRING,
    path:{type: DataTypes.STRING, allowNull: true,
      get: function () {
          if(this.getDataValue('path')){

            if(this.getDataValue('specialPackage_id'))
            {
              return CONFIG.filePath +'/images/offer/'+ this.getDataValue('specialPackage_id') +'/'+ this.getDataValue('path');
            }else{
              return CONFIG.filePath +'/images/restaurant/'+ this.getDataValue('restaurant_id') +'/'+ this.getDataValue('filename')+'/'+ this.getDataValue('path');
            }
              
          }else if(this.getDataValue('path') === undefined)
              return ;
          else 
              return CONFIG.filePath + '/images/media/default.png';
          //return null;
      }},
    thumbnail_path:{type: DataTypes.STRING, allowNull: true,
      get: function () {
          if(this.getDataValue('thumbnail_path')){

            if(this.getDataValue('specialPackage_id'))
            {
              return CONFIG.filePath +'/images/offer/'+ this.getDataValue('specialPackage_id') +'/'+ this.getDataValue('thumbnail_path');
            }else{
              return CONFIG.filePath +'/images/restaurant/'+ this.getDataValue('restaurant_id') +'/'+ this.getDataValue('filename')+'/'+ this.getDataValue('thumbnail_path');
            }
              
          }else if(this.getDataValue('thumbnail_path') === undefined)
              return ;
          else 
              return null;
      }},
    media_usage:DataTypes.STRING,
    checksum:DataTypes.STRING,
    is_cover:DataTypes.BOOLEAN,
    is_profile:DataTypes.BOOLEAN,
    caption:DataTypes.STRING,
    display_index:DataTypes.INTEGER

  },  {
    freezeTableName: true,
    tableName: 'media',
    timestamps: true,

  });

  Media.associate = function(models) {
    //this.restaurant_id = this.belongsTo(models.restaurant, {foreignKey: 'restaurant_id'});
    this.specialPackage_id = this.belongsTo(models.SpecialPackage, {foreignKey: 'specialPackage_id'});
  };

  return Media;

};